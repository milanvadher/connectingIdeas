import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import * as firebase from 'firebase';

@Injectable()
export class AuthProvider {

  public Student: string = "Student";
  userProfile: firebase.database.Reference;
  currentUser: firebase.User;

  constructor(public http: Http) {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.currentUser = user;
        this.userProfile = firebase.database().ref(`/userProfile/${user.uid}/userData/`);
      }
    });
    console.log('Hello AuthProvider Provider');
  }

  loginUser(email: string, password: string): Promise<any> {
    if (this.currentUser.emailVerified === true) {
      return firebase.auth().signInWithEmailAndPassword(email, password);
    } else {
      alert("please confirm your mail id ...");
    }

  }

  signupStudent(email: string, password: string, fname: string, lname: string, branch: string): Promise<any> {
    return firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(newUser => {
        newUser.sendEmailVerification().then(() => {
          alert("Send you a mail for confirm your e-mail ID ...");
          firebase.database().ref('/userProfile').child(newUser.uid).child('/userData/')
            .set({ email: email, fname: fname, lname: lname, branch: branch });
        })
      });
  }

  signupOther(email: string, password: string, fname: string, lname: string): Promise<any> {
    return firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(newUser => {
        firebase.database().ref('/userProfile').child(newUser.uid).child('/userData/')
          .set({ email: email, fname: fname, lname: lname, register: this.Student });
      });
  }

  resetPassword(email: string): Promise<void> {
    return firebase.auth().sendPasswordResetEmail(email);
  }

  logoutUser(): Promise<void> {
    return firebase.auth().signOut();
  }

}
