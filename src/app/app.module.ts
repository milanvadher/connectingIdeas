import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Camera } from '@ionic-native/camera';

import { MyApp } from './app.component';
import { FeedsPage } from "../pages/feeds/feeds";
import { ProfilePage } from "../pages/profile/profile";
import { SaffronyPage } from "../pages/saffrony/saffrony";
import { UploadPage } from "../pages/upload/upload";
import { TabsPage } from "../pages/tabs/tabs";
import { AboutUsPage } from "../pages/about-us/about-us";
import { FeedbackPage } from "../pages/feedback/feedback";
import { LoginPage } from "../pages/login/login";
import { ValidatorPage } from "../pages/validator/validator";
import { PasswordResetPage } from "../pages/password-reset/password-reset";
import { SignUpPage } from "../pages/sign-up/sign-up";

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AuthProvider } from '../providers/auth/auth';
import { HttpModule } from '@angular/http';
import { ProfileProvider } from '../providers/profile/profile';
import { DatabaseProvider } from '../providers/database/database';
import { AngularFireDatabase } from 'angularfire2/database-deprecated';

export const firebaseConfig = {
  apiKey: "AIzaSyD-3smA3LPFl_UJ2MpcjCnhKSRIm7dj3Ms",
    authDomain: "saffrony-524ca.firebaseapp.com",
    databaseURL: "https://saffrony-524ca.firebaseio.com",
    projectId: "saffrony-524ca",
    storageBucket: "saffrony-524ca.appspot.com",
    messagingSenderId: "235588142857"
};

@NgModule({
  declarations: [
    MyApp,
    ProfilePage,
    SaffronyPage,
    UploadPage,
    FeedsPage,
    TabsPage,
    AboutUsPage,
    FeedbackPage,
    LoginPage,
    ValidatorPage,
    PasswordResetPage,
    SignUpPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ProfilePage,
    SaffronyPage,
    UploadPage,
    FeedsPage,
    TabsPage,
    AboutUsPage,
    FeedbackPage,
    LoginPage,
    ValidatorPage,
    PasswordResetPage,
    SignUpPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    HttpModule,
    Camera,
    ProfileProvider,
    DatabaseProvider,
    AngularFireDatabase
  ]
})
export class AppModule {}
