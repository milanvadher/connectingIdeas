import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TabsPage } from "../pages/tabs/tabs";

import { AngularFireAuth } from "angularfire2/auth";
import { AboutUsPage } from "../pages/about-us/about-us";
import { FeedbackPage } from "../pages/feedback/feedback";
import { LoginPage } from "../pages/login/login";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage: any;

  @ViewChild(Nav) nav: Nav;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private afAuth: AngularFireAuth) {
    platform.ready().then(() => {

      this.afAuth.authState.subscribe(auth => {
        if (!auth) {
          this.rootPage = LoginPage;
        }
        else {
          this.rootPage = TabsPage;
        }
      })

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

  }

  openAboutUs() {
    this.nav.push(AboutUsPage);
  }

  openFeedback() {
    this.nav.push(FeedbackPage)
  }

  logout() {
    this.afAuth.auth.signOut();
    this.nav.setRoot(LoginPage);
  }

}

