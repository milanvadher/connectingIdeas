import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { DatabaseProvider } from "../../providers/database/database";
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';

@Component({
  selector: 'page-feeds',
  templateUrl: 'feeds.html',
})
export class FeedsPage {
  userFeeds: FirebaseListObservable<any[]>;;

  constructor(public navCtrl: NavController, public af: AngularFireDatabase, public navParams: NavParams, public platform: Platform) {
    this.userFeeds = this.af.list('/moderator');
    console.log(this.userFeeds)
  }

  ionViewDidLoad() {
  }

  like($key, like) {
    this.userFeeds.update($key, { likes: like + 1 });
  }

}
