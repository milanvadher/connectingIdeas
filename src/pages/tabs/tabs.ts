import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FeedsPage } from "../feeds/feeds";
import { SaffronyPage } from "../saffrony/saffrony";
import { ProfilePage } from "../profile/profile";
import { UploadPage } from "../upload/upload";

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  tab1Root = FeedsPage;
  tab2Root = UploadPage;
  tab3Root = SaffronyPage;
  tab4Root = ProfilePage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

}
