import { Component } from '@angular/core';
import { NavController, NavParams, Loading, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { ValidatorPage } from '../validator/validator';
import { TabsPage } from "../tabs/tabs";
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {

  public signupFormStudent: FormGroup;
  public signupFormOther: FormGroup;
  loading: Loading;

  constructor(public navCtrl: NavController, public navParams: NavParams, public authProvider: AuthProvider, public formBuilder: FormBuilder, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
    this.signupFormStudent = formBuilder.group({
      fname: ['', Validators.compose([Validators.minLength(2), Validators.required])],
      lname: ['', Validators.compose([Validators.minLength(2), Validators.required])],
      branch: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required, ValidatorPage.isValid])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      passwordRetyped: ['', Validators.compose([Validators.minLength(6), Validators.required])]
    });
    this.signupFormOther = formBuilder.group({
      fname: ['', Validators.compose([Validators.minLength(2), Validators.required])],
      lname: ['', Validators.compose([Validators.minLength(2), Validators.required])],
      email: ['', Validators.compose([Validators.required, ValidatorPage.isValid])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      passwordRetyped: ['', Validators.compose([Validators.minLength(6), Validators.required])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignUpPage');
  }

  signupStudent(){
    
    if (this.signupFormStudent.value.password !== this.signupFormStudent.value.passwordRetyped) {
      let alert = this.alertCtrl.create({
        title: 'Error',
        message: 'Your password and your re-entered password does not match each other.',
        buttons: ['OK']
      });
      alert.present();
      return;
    }

    if (!this.signupFormStudent.valid){
      console.log(this.signupFormStudent.value);
    } else {
      this.authProvider.signupStudent(this.signupFormStudent.value.email, this.signupFormStudent.value.password, this.signupFormStudent.value.fname, this.signupFormStudent.value.lname, this.signupFormStudent.value.branch)
      .then(() => {
        this.loading.dismiss().then( () => {
          this.navCtrl.setRoot(LoginPage);
        });
      }, (error) => {
        this.loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: error.message,
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          alert.present();
        });
      });
      this.loading = this.loadingCtrl.create();
      this.loading.present();
    }
  }

  signupOther(){
    
    if (this.signupFormOther.value.password !== this.signupFormOther.value.passwordRetyped) {
      let alert = this.alertCtrl.create({
        title: 'Error',
        message: 'Your password and your re-entered password does not match each other.',
        buttons: ['OK']
      });
      alert.present();
      return;
    }

    if (!this.signupFormOther.valid){
      console.log(this.signupFormOther.value);
    } else {
      this.authProvider.signupOther(this.signupFormOther.value.email, this.signupFormOther.value.password, this.signupFormOther.value.fname, this.signupFormOther.value.lname)
      .then(() => {
        this.loading.dismiss().then( () => {
          this.navCtrl.setRoot(LoginPage);
        });
      }, (error) => {
        this.loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: error.message,
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          alert.present();
        });
      });
      this.loading = this.loadingCtrl.create();
      this.loading.present();
    }
  }

}
