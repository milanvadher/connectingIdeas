import { Component } from '@angular/core';
import { NavController, NavParams, Loading, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ValidatorPage } from '../validator/validator';
import { AuthProvider } from '../../providers/auth/auth';
import { TabsPage } from "../tabs/tabs";
import { PasswordResetPage } from "../password-reset/password-reset";
import { SignUpPage } from "../sign-up/sign-up";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public loginForm:FormGroup;
  public loading:Loading;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public alertCtrl: AlertController, 
    public authProvider: AuthProvider, public formBuilder: FormBuilder) {

      this.loginForm = formBuilder.group({
        email: ['', Validators.compose([Validators.required, 
          ValidatorPage.isValid])],
        password: ['', Validators.compose([Validators.minLength(6), 
          Validators.required])]
      });

  }

  loginUser(): void {
    if (!this.loginForm.valid){
      console.log(this.loginForm.value);
    } else {
      this.authProvider.loginUser(this.loginForm.value.email, 
          this.loginForm.value.password)
      .then( authData => {
        this.loading.dismiss().then( () => {
          this.navCtrl.setRoot(TabsPage);
        });
      }, error => {
        this.loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: error.message,
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          alert.present();
        });
      });
      this.loading = this.loadingCtrl.create({
        content: "Please wait..."
      });
      this.loading.present();
    }
  }

  goToSignup(): void { this.navCtrl.push(SignUpPage); }
  
  goToResetPassword(): void { this.navCtrl.push(PasswordResetPage); }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
